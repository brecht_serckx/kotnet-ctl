# kotnet-ctl
CLI tool for logging in and out kotnet

System requirements:
- cURL
- Linux (Mac and WSL untested, but probably working)

```
Usage: kotnet-ctl COMMAND PARAMS
Commands:
  help                        Shows help
  login USERNAME PASSWORD     Log in to KotNet with the provided credentials
  logout USERNAME PASSWORD    Log out of KotNet 
  monitor USERNAME PASSWORD   Keep KotNet connection logged in (log back in when your session is expired)
 ```
